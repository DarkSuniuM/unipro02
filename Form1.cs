﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int students_count = Convert.ToInt32(textBox1.Text);
            String course_title = comboBox1.Text;
            float avg = 0;
            for (int i = 0; i < students_count; i++)
            {
                var get_score = new GetScore();
                get_score.ShowDialog();
                if (get_score.result == 0)
                {
                    continue;
                }
                avg += get_score.result / students_count;
            }
            MessageBox.Show("Average score of " + students_count.ToString() + " students for " + course_title + " is " + avg.ToString() + "!");
        }

        private void TextBox1_KeyUp(object sender, KeyEventArgs e)
        {
            this.validateInputs();
        }

        private void validateInputs()
        {
            if (!int.TryParse(textBox1.Text, out int output) || string.IsNullOrEmpty(comboBox1.Text))
            {
                button1.Enabled = false;
            } else {
                button1.Enabled = true;
            }
        }

        private void ComboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            this.validateInputs();
        }
    }
}
