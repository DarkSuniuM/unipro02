﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project02
{
    public partial class GetScore : Form
    {
        public float result;
        public GetScore()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.result = float.Parse(textBox1.Text);
            this.Close();
        }

        private void TextBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (!float.TryParse(textBox1.Text, out float output))
            {
                textBox1.BackColor = Color.IndianRed;
                button1.Enabled = false;
            } else
            {
                textBox1.BackColor = Color.GreenYellow;
                button1.Enabled = true;
            }
        }

        private void TextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (float.TryParse(textBox1.Text, out float output))
                {
                    this.result = float.Parse(textBox1.Text);
                    this.Close();
                }
            }
        }
    }
}
