Form1
#####

- NameSpace: `Project02`
- Form Design: ``Form1.Designer.cs``
- Form Logic: ``Form1.cs``

Classes
*******

Form1 (Form)
============

The main form of application,
The :doc:`GetScore` form loads within this form.

Methods
---------

``public Form1()``
^^^^^^^^^^^^^^^^^^
Description:
 This method initializes the form.

``private void Button1_Click(object sender, EventArgs e)``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Description:
 This method gets called when user clicks on the ``Button1`` in the form.
 By default the button is disabled, till the user provides the valid input!
Behavior:
 Declares 3 local variables:

 :students_count: Type: ``Integer``, Value: ``textBox1``'s value as an Integer
 :course_title: Type: ``String``, Value: Chosen item's label from ``comboBox1``
 :avg: Type: ``Float``, Value: It's 0 by default

 Runs a loop on students_count value,
 for each time it interprets loop's body,
 It creates another variable named ``get_score``,
 this variable is an object of :doc:`GetScore` form,
 It shows the new form as a dialog to the user
 and waits for user input (a student score).

 After user provides the input,
 it checks the other form's ``result`` attribute,
 if it's providen as 0,
 then it doesn't do anything than continuing to the next time of loop,
 If it's anything other than 0,
 it calculates ``get_score.result`` divided by ``students_count``
 , adds it to the ``avg`` value and store it at the same variable (``avg``).

 When the loop is done,
 it show a ``MessageBox`` that contains ``students_count``,
 ``course_title`` and the ``avg``!

``private void TextBox1_KeyUp(object sender, KeyEventArgs e)``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Description:
 This method gets called when user press and release a key on ``textBox1``
Behavior:
 It runs `this.validateInputs() <#private-void-validateinputs>`_ method.

``private void ComboBox1_SelectedValueChanged(object sender, EventArgs e)``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Description:
 This method gets called when user changes the chosen value on ``comboBox1``
Behavior:
 It runs `this.validateInputs() <#private-void-validateinputs>`_ method.

.. _validateInputs:

``private void validateInputs()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Description:
 Validates user inputs on ``textBox1`` and ``comboBox1``
Behavior:
 Checks the following conditions:
    - ``textBox1.Text`` value is not a valid ``Integer``
    - ``comboBox1.Text`` is null or empty

 If one or both of the conditions become ``true``,
 it will disable ``button1``, Otherwise it will enable it!
