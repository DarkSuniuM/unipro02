GetScore
########

- NameSpace: `Project02`
- Form Design: ``GetScore.Designer.cs``
- Form Logic: ``GetScore.cs``

Classes
*******

GetScore (Form)
===============

This form will get opened within the :doc:`Form1`
to get the user input for each student.

Attributes
----------
:result: ``Float``, stores the ``textBox1`` value

Methods
---------

``public GetScore()``
^^^^^^^^^^^^^^^^^^^^^
Description:
 This method initializes the form.

``private void Button1_Click(object sender, EventArgs e)``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Description:
 This method will get called when the user clicks on ``Button1``,
 It's disabled by default!
Behavior:
 Assigns ``textBox1.Text`` value as a float to ``result`` attribute
 and closes the open instance of the form.

``private void TextBox1_KeyUp(object sender, KeyEventArgs e)``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Description:
 This method will get called when user releases a key,
 It validates the user input.
Behavior:
 It checks if user input on ``textBox1``
 is not a valid ``Integer`` or ``Float``,
 If the condition was ``true``,
 it disables ``button1`` and changes the ``textBox1`` background color to red,
 otherwise it enabled ``button1`` and
 changes the ``textBox1`` background color to green.

``private void TextBox1_KeyPress(object sender, KeyPressEventArgs e)``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Description:
 This method will get called when user presses a key,
 It tries to submit the value if the key was Enter.
Behavior:
 If checks if the ``keyPress.KeyChar`` is equal to ``13`` (Enter key),
 If it wasn't, it won't do anything, but if it was, It validates the user input
 and closes the opened instance of the form.
